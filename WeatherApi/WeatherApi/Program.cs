﻿using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherApi.Api;

namespace WeatherApi
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string url = "http://api.openweathermap.org/data/2.5/weather";
            var client = RestClient.For<IWeatherClient>(url);
            var key = "f659bece147232554fbb98c443dae93b";

            var response1 = await client.GetWeather("Poznan", "pl", key);

            Console.WriteLine(response1.ToString());
            Console.ReadKey();
        }
    }
}
