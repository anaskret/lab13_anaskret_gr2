﻿using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherApi.Models;

namespace WeatherApi.Api
{
    public interface IWeatherClient
    {
        [Get]
        Task<WeatherForecasts> GetWeather([Query("q")] string city, string lang, string apiKey);
    }
}
