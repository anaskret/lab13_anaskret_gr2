﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApi.Models
{
    public class WeatherForecasts
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        //public WeatherInfo Weather { get; set; }

        public override string ToString()
        {
            return $"Temp: {Weather.Temp}, ID: {Id}, Name: {Name}";
        }
    }
}
