﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApi.Models
{
    public class WeatherInfo
    {
        [JsonProperty("temp")]
        public double Temp { get; set; }
    }
}
